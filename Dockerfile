FROM google/cloud-sdk:272.0.0-alpine

RUN apk --no-cache add wget=1.20.3-r0

RUN wget https://get.helm.sh/helm-v3.0.1-linux-amd64.tar.gz
RUN tar -zxvf helm-v3.0.1-linux-amd64.tar.gz
RUN chmod +x linux-amd64/helm
RUN mv linux-amd64/helm /usr/local/bin/helm
RUN rm -rf linux-amd64/
RUN rm -rf helm-v3.0.1-linux-amd64.tar.gz

RUN wget https://github.com/roboll/helmfile/releases/download/v0.94.1/helmfile_linux_amd64
RUN chmod +x ./helmfile_linux_amd64
RUN mv ./helmfile_linux_amd64 /usr/local/bin/helmfile
